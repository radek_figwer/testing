import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VerifyEmployee extends EmployeeData{


    //I am able to create an employee:

    @Test
    public void testEmployee(){

        setName("Eva");
        setSalary(12000);
        setAge(22);

        String name = getName();
        double salary = getSalary();
        int age = getAge();

        String requestBody = "{\"name\":\""+ name +"\",\"salary\":\""+ salary +"\",\"age\":\""+ age +"\"}";
        //System.out.println(name + salary + age);
        //System.out.println(requestBody);


        Response resp = RestAssured.given().
                contentType("application/json").
                //body("{\"name\":\"Tim\",\"salary\":\"11000\",\"age\":\"22\"}").
                body(requestBody).
                when().
                post("http://dummy.restapiexample.com/api/v1/create");

        //check
        String body = resp.getBody().asString();
        System.out.println(body);


    }

//I am able to create an employee and get its id, but not able to use employee/{id} endpoint

    //create new employee and get its id
    @Test
    public void testEmployee2(){
        setName("Eva");
        setSalary(12000);
        setAge(22);

        String name = getName();
        double salary = getSalary();
        int age = getAge();

        String requestBody = "{\"name\":\""+ name +"\",\"salary\":\""+ salary +"\",\"age\":\""+ age +"\"}";

        int id = RestAssured.given().
                contentType("application/json").
                body(requestBody).
                when().
                post("http://dummy.restapiexample.com/api/v1/create").
                then().
                extract().
                path("data.id");

        System.out.println(id);

        //use endpoint: http://dummy.restapiexample.com/api/v1/employee/26

        Response res = RestAssured.get("http://dummy.restapiexample.com/api/v1/employee/"+id+"");
        assertEquals(200, res.getStatusCode());
        String json = res.asString();
        JsonPath jp = new JsonPath(json);
        System.out.println(json);
        assertEquals(""+id+"", jp.get("data.id"));

    }



    //HOWEVER THIS ENDPOINT WORKS: http://dummy.restapiexample.com/api/v1/employees
    @Test
    public void testEmployee3(){

        Response res = RestAssured.get("http://dummy.restapiexample.com/api/v1/employees");
        assertEquals(200, res.getStatusCode());
        String json = res.asString();
        JsonPath jp = new JsonPath(json);
        System.out.println(json);
        assertEquals("success", jp.get("status"));
        assertEquals("Bradley Greer", jp.get("data[18].employee_name"));
    }

/*
    I found out that:
    - in Postman everything works, but I see that a new employee has an int id while existing employees have String id
    - if I paste to browser this endpoint url http://dummy.restapiexample.com/api/v1/employees - it shows json, but if I paste http://dummy.restapiexample.com/api/v1/employee/{specific-id}" - accept cookies dialog box appears
    - an employee is created for a limited time (which is ok).
*/

}



